package controllers

import (
	"net/http"

	"github.com/fullstack/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Wellcome to this awesome API")
}
